const { Model } = require('objection');

class Task extends Model {
  static get tableName() {
    return 'tasks';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['name', 'author'],

      properties: {
        id: { type: 'integer' },
        name: { type: 'string' },
        author: { type: 'string' }
      }
    };
  }




}

module.exports = Task;