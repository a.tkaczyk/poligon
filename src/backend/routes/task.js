const Joi = require('@hapi/joi');
// const {
//   createTag,
//   updateTag,
//   deleteTag
// } = require('../services/tasksService');
const { getTasks } = require('../readers/taskReader');

module.exports = [
  {
    method: 'GET',
    path: '/tasks',
    config: {
      auth: false,
      description: 'Get all tasks from DB',
      notes: 'Get tasks',
      tags: ['api'], //error?
      validate: {
        query: Joi.object({
          page: Joi.number()
            .integer()
            .optional(),
          perPage: Joi.number()
            .integer()
            .optional(),
          query: Joi.string().optional()
        })
      }
    },
    handler: async ({ db, query }, h) => {
      // return 'Task here';
      return getTasks(db, query);
    }
  }
];
