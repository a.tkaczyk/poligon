const Hapi = require('@hapi/hapi');
// const Joi = require('@hapi/joi');
const routes = require('./routes/index');

const init = async () => {
  const server = Hapi.server({
    port: 3000,
    host: 'localhost'
  });

  server.route(routes);

  server.route({
    method: '*',
    path: '/{any*}',
    handler: function(request, h) {
      return '404 Error! Page Not Found!';
    }
  });

  await server.start();
  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', err => {
  console.log(err);
  process.exit(1);
});

init();
