const knex = require('knex');
const { transaction, knexSnakeCaseMappers } = require('objection')
// const { db } = require('../config/config')()
const db = {
  db: {
    client: 'pg',
    debug: false,
    asyncStackTraces: false,
    connection: {
      user: 'postgres',
      database: 'dbtest1',
      password: 'newPassword',
      port: 5432
    }
  }
};

exports.transaction = transaction
exports.initDatavase() = () => {
  let trx = null
  const connection = knex({ ...db, ...knexSnakeCaseMappers() })

  return {
    getKnex() {
      if (trx) {
        return trx
      }
      return connection
    },

    async startTransaction() {
      trx = await transaction.start(connection)
      return trx
    },
    async rollbackTransaction() {
      await trx.rollback()
      trx = null
    },
    async destroy() {
      await connection.destroy()
    }
  }
}


