const Tasks = require('../model/Task')

exports.getTasks = async (db, { query, page = 0, perPage = 10 }) => {
  const qb = Tasks.query(db)

  if (query) {
    qb.where('name', 'like', `%${query}%`)
  }

  return qb.orderBy('id', 'ASC').page(page, perPage)
}
